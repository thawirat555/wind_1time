from netCDF4 import Dataset, num2date
import json
import math


nc = Dataset('wrfout_d01.nc')
print(nc.variables.keys())


lats = nc.variables['lat'][:]
lons = nc.variables['lon'][:]
press = nc.variables['pressure'][:]

u_var = 'u_tr_p'
v_var = 'v_tr_p'

_data_u = nc.variables[u_var][0][0]
_data_v = nc.variables[v_var][0][0]


param_name_u = nc.variables[u_var].standard_name
param_name_v = nc.variables[v_var].standard_name
param_unit = nc.variables[u_var].units

# ค่าสุดท้ายของ latitude
la1 = float('%.3f'%lats[lats.shape[0]-1])
# ค่าแรกของ longitude
lo1 = float('%.3f'%lons[0])
# ค่าแรกของ latitude
la2 = float('%.3f'%lats[0])
# ค่าสุดท้ายของ longitude
lo2 = float('%.3f'%lons[lons.shape[0]-1])
# จำนวนจุดของ latitude
nx = int(lats.shape[0])
# จำนวนจุดของ longitude
ny = int(lons.shape[0])
# จำนวนจุดทั้งหมด หรือจำนวนดาต้า
tot = nx * ny
# ระยะห่างของจุด ใน latitude
dx = math.fabs(float('%.3f'%((la1 - la2)/nx)))
# ระยะห่างของจุด ใน longitude
dy = math.fabs(float('%.3f'%((lo1 - lo2)/ny)))


_output_u = []
_output_v = []
# วนลูปตาม latitude
for lat in range(len(lats)):
    # วนลูปตาม longitude
    for lon in range(len(lons)):
        # เช็คดาต้า ถ้าดาต้าตัวที่ lat lon นั้นไม่มีให้เพิ่มค่าเป็น None
        if math.isnan(float(_data_u[lat][lon])) or math.isnan(float(_data_v[lat][lon])) == True :
            _output_u.append(None)
            _output_v.append(None)
        else:
            _output_u.append(float('%.3f'%_data_u[lat][lon]))
            _output_v.append(float('%.3f'%_data_v[lat][lon]))

# ประกาศตัวแปรของโครงสร้าง json ลมแบบ vecter
wind_data = [{
    "header": {
        "parameterNumberName": param_name_u,
        "parameterUnit": param_unit,
        "parameterNumber": 2,
         "parameterCategory": 2,
        "nx": nx,
        "ny": ny,
        "numberPoints": tot,
        "dx": dx,
        "dy": dy,
        "la1": la1,
        "lo1": lo1,
        "la2": la2,
        "lo2": lo2,
    },
    "data": _output_u
    }, {
    "header": {
        "parameterNumberName": param_name_v,
        "parameterUnit": param_unit,
        "parameterNumber": 3,
        "parameterCategory": 2,
        "nx": nx,
        "ny": ny,
        "numberPoints": tot,
        "dx": dx,
        "dy": dy,
        "la1": la1,
        "lo1": lo1,
        "la2": la2,
        "lo2": lo2,
    },
    "data": _output_v
}]

# ประกาศชื่อไฟล์
output_file_name = "wind.json"
    # สร้างไฟล์ json ตามชื่อบรรทัดบน และเพิ่มข้อมูลลงไป
with open(output_file_name, 'w') as output_file:  
    json.dump(wind_data, output_file, separators=(',', ':'))
# เมื่อสร้างไฟล์เสร็จ 1 ไฟล์ โชว์ข้อความ Success writing json
print ('[INFO] *** SUCCESS writing JSON Wind ***')