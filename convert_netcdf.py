
# ============================================================================================================
# ในส่วนนี้จะเป็นการเรียกใช้ library ต่างๆของ python มาใช้ สำหรับโค้ดนี้
# จำเป็นจะต้อง install ทั้งหมด โดยใช้คำสั่งดังนี้
# pip install Dataset
# pip install numpy
# pip install json
# pip install math
# ปล.เฉพาะการรันโค้ดครั้งแรก

from netCDF4 import Dataset, num2date
import json
import math
import os
import argparse 

FLAGS = None

def main():

  # ============================================================================================================
  # อ่านไฟล์ netcdf ที่จะแปลงเป็น json โดยใช้ library Dataset
  # เปลี่ยนชื่อให้ตรงกับชื่อไฟล์ที่ต้องการแปลง

  nc = Dataset(FLAGS.input_file)

  # ============================================================================================================
  # ในส่วนนี้คือตัวแปรสำคัญ โดยจะต้องตั้งค่า variables ต่างๆให้ตรงกับ variables ของไฟล์ netcdf ข้างบน
  # โดยดู variables จากโปรแกรมอ่าน netcdf ต่างๆ หรือโค้ดบรรทัดล่าง
  print(nc.variables.keys())

  # variable พื้นฐานที่จำเป็นต้องใช้ latitude longitude time และ pressure สำหรับลม 
  lats = nc.variables['lat'][:]
  lons = nc.variables['lon'][:]
  press = nc.variables['pressure'][:]
  times = nc.variables['time']
  units = nc.variables['time'].units

  # แปลงค่า time ในอยู่ในรูปแบบ ปีเดือนวันชัวโมง
  dates = num2date(times[:], units=units, calendar='365_day')
  datetime = ([date.strftime('%Y%m%d%H') for date in dates])


  # 2 ค่านี้จะใช้เฉพาะ variable ของลม ใช้สำหรับแปลงค่าออกมาเป็น vecter 
  # ซึ่งจำเป็นจะต้องใช้ทั้ง u และ v
  u_var = FLAGS.u_data
  v_var = FLAGS.v_data
  # ============================================================================================================


  # ============================================================================================================
  # ------------------------ สำหรับดาต้าล้ม ที่เป็น vecter ----------------------------------------------------------

  # ประกาศตัวแปร และคำนวณค่าต่างๆ สำหรับ header ของดาต้าลม

  param_name_u = nc.variables[u_var].standard_name
  param_name_v = nc.variables[v_var].standard_name
  param_unit = nc.variables[u_var].units

  # ค่าสุดท้ายของ latitude
  la1 = float('%.3f'%lats[lats.shape[0]-1])
  # ค่าแรกของ longitude
  lo1 = float('%.3f'%lons[0])
  # ค่าแรกของ latitude
  la2 = float('%.3f'%lats[0])
  # ค่าสุดท้ายของ longitude
  lo2 = float('%.3f'%lons[lons.shape[0]-1])
  # จำนวนจุดของ latitude
  nx = int(lats.shape[0])
  # จำนวนจุดของ longitude
  ny = int(lons.shape[0])
  # จำนวนจุดทั้งหมด หรือจำนวนดาต้า
  tot = nx * ny
  # ระยะห่างของจุด ใน latitude
  dx = math.fabs(float('%.3f'%((la1 - la2)/nx)))
  # ระยะห่างของจุด ใน longitude
  dy = math.fabs(float('%.3f'%((lo1 - lo2)/ny)))

  # print('param_name:',param_name_v,param_name_u,'param_unit:',param_unit,'nx:',nx,'ny:',ny,'tot:',tot)
  # print('dx:',dx,'dy:',dy,'la1:',la1,'lo1:',lo1,'la2:',la2,'lo2:',lo2)

  # วนลูปตาม time
  for date in range(len(datetime)):
      print('process ',' in ' ,datetime[date])
      # วนลูปตาม pressure เพื่อสร้าง json แต่ละเวลา แต่ละ pressure
      for i,pres in enumerate(press):
          print('pressure ',pres,i)
          # อ่านไฟล์ netcdf ในแต่ละ time และ pressure
          _data_u = nc.variables[u_var][date][i]
          _data_v = nc.variables[v_var][date][i]
          # ประกาศตัวแปร สำหรับดาต้าล้ม
          _output_u = []
          _output_v = []
          # วนลูปตาม latitude
          for lat in range(len(lats)):
              # วนลูปตาม longitude
              for lon in range(len(lons)):
                  # เช็คดาต้า ถ้าดาต้าตัวที่ lat lon นั้นไม่มีให้เพิ่มค่าเป็น None
                  if math.isnan(float(_data_u[lat][lon])) or math.isnan(float(_data_v[lat][lon])) == True :
                      _output_u.append(None)
                      _output_v.append(None)
                  else:
                      _output_u.append(float('%.3f'%_data_u[lat][lon]))
                      _output_v.append(float('%.3f'%_data_v[lat][lon]))

          #ประกาศตัวแปรของโครงสร้าง json ลมแบบ vecter
          wind_data = [{
            "header": {
              "parameterNumberName": param_name_u,
              "parameterUnit": param_unit,
              "parameterNumber": 2,
              "parameterCategory": 2,
              "nx": nx,
              "ny": ny,
              "numberPoints": tot,
              "dx": dx,
              "dy": dy,
              "la1": la1,
              "lo1": lo1,
              "la2": la2,
              "lo2": lo2,
            },
            "data": _output_u
          }, {
            "header": {
              "parameterNumberName": param_name_v,
              "parameterUnit": param_unit,
              "parameterNumber": 3,
              "parameterCategory": 2,
              "nx": nx,
              "ny": ny,
              "numberPoints": tot,
              "dx": dx,
              "dy": dy,
              "la1": la1,
              "lo1": lo1,
              "la2": la2,
              "lo2": lo2,
            },
            "data": _output_v
          }]
          # เช็คว่ามี folder ลมอยู่หรือไม่ ถ้าไม่มีให้สร้าง folder ลม
          if not os.path.exists("%s/%s/wind/%s"%(FLAGS.output_dir,FLAGS.domain,pres)):
            os.makedirs("%s/%s/wind/%s"%(FLAGS.output_dir,FLAGS.domain,pres))

          # ประกาศชื่อไฟล์
          output_file_name = "%s/%s/wind/%s/wind_%s00.json"%(FLAGS.output_dir,FLAGS.domain,pres,datetime[date])
          # สร้างไฟล์ json ตามชื่อบรรทัดบน และเพิ่มข้อมูลลงไป
          with open(output_file_name, 'w') as output_file:  
              json.dump(wind_data, output_file, separators=(',', ':'))
          # เมื่อสร้างไฟล์เสร็จ 1 ไฟล์ โชว์ข้อความ Success writing json
          print ('[INFO] *** SUCCESS writing JSON Wind ***')


  # ============================================================================================================

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument(
      '--input_file',
      type=str,
      default='',
      help='Path file input'
  )
  parser.add_argument(
      '--domain',
      type=str,
      default='domain1',
      help='Path to folders of ouput'
  )
  parser.add_argument(
      '--v_data',
      type=str,
      default='',
      help='Variables name of v data'
  )
  parser.add_argument(
      '--u_data',
      type=str,
      default='',
      help='Variables name of u data'
  )
  parser.add_argument(
      '--output_dir',
      type=str,
      default='output',
      help='Path to folders of ouput'
  )
  FLAGS, unparsed = parser.parse_known_args()
  main()