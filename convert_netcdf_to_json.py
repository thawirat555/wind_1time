import sys
import json
import subprocess
import os


fileName = "wrfout_cf_d01.nc"



scalar_params = ['salt', 'temp', 'sound', 'w', 'zeta']
vector_params = ['u', 'v']

times = {}

if not os.path.exists("json_data"):
  os.makedirs("json_data")
if not os.path.exists("json_data/domain1"):
  os.makedirs("json_data/domain1")
if not os.path.exists("json_data/domain2"):
  os.makedirs("json_data/domain2")
if not os.path.exists("json_data/domain3"):
  os.makedirs("json_data/domain3")

domain = "wrfout_cf_d01.nc"

for name in scalar_params:
  cmd = '''gdalinfo NETCDF:%s:%s | grep NETCDF_DIM_ocean_time= | grep -Eo '[0-9]{1,}' > %s_time.txt'''%(fileName, name, name)
  subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True).wait()
  with open('%s_time.txt'%name, 'r+') as f:
    times['%s_time'%name] = f.read().splitlines()

cmd = '''gdalinfo NETCDF:%s:u | grep NETCDF_DIM_ocean_time= | grep -Eo '[0-9]{1,}' > uv_time.txt'''%(fileName)
subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True).wait()
with open('uv_time.txt', 'r+') as f:
  times["uv_time"] = f.read().splitlines()

for param in scalar_params:
  if(param!='zeta'):
    numberOfband = 721
  else:
    numberOfband = 4

  for bandNumber in range(0, len(times['%s_time'%param])):
    band_counter = bandNumber+1
    if((band_counter % 30) == 0):
      b = 30
    else:
      b = 0
    translate_band_cmd = '''gdal_translate -of GMT NETCDF:%s:%s %s_%d.nc -b %d''' %(fileName, param, param, band_counter, band_counter)
    json_dump_cmd = "ncdump-json -j %s_%d.nc > %s_%d.json" %(param, band_counter, param, band_counter)
    rm_u_cmd = "rm -f %s_%d.nc*"%(param, band_counter)
    subprocess.Popen(translate_band_cmd, stdout=subprocess.PIPE, shell=True).wait()
    subprocess.Popen(json_dump_cmd, stdout=subprocess.PIPE, shell=True).wait()
    subprocess.Popen(rm_u_cmd, stdout=subprocess.PIPE, shell=True).wait()

    with open("%s_%d.json"%(param, band_counter)) as data_file:  
      data = json.load(data_file)
      dict =  [{
      "header": {
        "nx": data["dimension"][0],
        "ny": data["dimension"][1],
        "lo1": data["x_range"][0],
        "la1": data["y_range"][1],
        "lo2": data["x_range"][1],
        "la2": data["y_range"][0],
        "dx": data["spacing"][0],
        "dy": data["spacing"][1]
        },
        "data" : data["z"]
        }]
    if (param != 'zeta'):
      output_file_name = "json_data/domain%s/%s_%s_%d.json"%(domain, param, 946659600000 + (eval(times['%s_time'%param][band_counter-1]) * 1000), (band_counter%30)+b)
    else:
      output_file_name = "json_data/domain%s/%s_%s.json"%(domain, param, 946659600000 + (eval(times['%s_time'%param][band_counter-1]) * 1000))
    with open(output_file_name, 'w') as output_file:
      output_file.write(json.dumps(dict))
    rm_json = "rm -f %s*.json"%param
    subprocess.Popen(rm_json, stdout=subprocess.PIPE, shell=True).wait()
    
for bandNumber in range(0,len(times['uv_time'])):
  band_counter = bandNumber+1
  if((band_counter % 30) == 0):
    b = 30
  else:
    b = 0
  u_param = '''gdal_translate -of GMT NETCDF:%s:u u_%d.nc -b %d''' %(fileName, band_counter, band_counter)
  json_dump_u_cmd = "ncdump-json -j u_%d.nc > u_%d.json" %(band_counter, band_counter)
  rm_u_cmd = "rm -f u_%d.nc*"%(band_counter)
  subprocess.Popen(u_param, stdout=subprocess.PIPE, shell=True).wait()
  subprocess.Popen(json_dump_u_cmd, stdout=subprocess.PIPE, shell=True).wait()
  subprocess.Popen(rm_u_cmd, stdout=subprocess.PIPE, shell=True).wait()

  v_param = '''gdal_translate -of GMT NETCDF:%s:v v_%d.nc -b %d''' %(fileName, band_counter, band_counter)
  json_dump_v_cmd = "ncdump-json -j v_%d.nc > v_%d.json" %(band_counter, band_counter)
  rm_v_cmd = "rm -f v_%d.nc*"%(band_counter)
  subprocess.Popen(v_param, stdout=subprocess.PIPE, shell=True).wait()
  subprocess.Popen(json_dump_v_cmd, stdout=subprocess.PIPE, shell=True).wait()
  subprocess.Popen(rm_v_cmd, stdout=subprocess.PIPE, shell=True).wait()

  with open("u_%d.json"%band_counter) as data_file_u, open("v_%d.json"%band_counter) as data_file_v:   
    data_u = json.load(data_file_u)
    data_v = json.load(data_file_v)

    dict =  [
    {
    "header": {
      "nx": data_u["dimension"][0],
      "ny": data_u["dimension"][1],
      "lo1": data_u["x_range"][0],
      "la1": data_u["y_range"][1],
      "lo2": data_u["x_range"][1],
      "la2": data_u["y_range"][0],
      "dx": data_u["spacing"][0],
      "dy": data_u["spacing"][1]
      },
      "data" : data_u["z"]
      },
      {
    "header": {
      "nx": data_v["dimension"][0],
      "ny": data_v["dimension"][1],
      "lo1": data_v["x_range"][0],
      "la1": data_v["y_range"][1],
      "lo2": data_v["x_range"][1],
      "la2": data_v["y_range"][0],
      "dx": data_v["spacing"][0],
      "dy": data_v["spacing"][1]
      },
      "data" : data_v["z"]
      }
      ]
    output_file_name = "json_data/domain%s/uv_%s_%d.json"%( domain, 946659600000 + (eval(times["uv_time"][band_counter-1]) * 1000), (band_counter%30)+b)
    with open(output_file_name, 'w') as output_file:
      output_file.write(json.dumps(dict))
    rm_json = "rm -f u*.json | rm -f v*.json"
    subprocess.Popen(rm_json, stdout=subprocess.PIPE, shell=True).wait()

rm_txt = "rm -f *.txt"
subprocess.Popen(rm_txt, stdout=subprocess.PIPE, shell=True).wait()