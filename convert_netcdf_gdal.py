#!/usr/bin/env python3 
# -*- coding: utf-8 -*-

import sys
import json
import subprocess
import os
from netCDF4 import Dataset, num2date
import argparse 

FLAGS = None

def main():

    # ============================================================================================================
    # อ่านไฟล์ netcdf ที่จะแปลงเป็น json โดยใช้ library Dataset
    # เปลี่ยนชื่อให้ตรงกับชื่อไฟล์ที่ต้องการแปลง

    dataset = Dataset(FLAGS.input_file)
    
    # โดยดู variables จากโปรแกรมอ่าน netcdf ต่างๆ หรือโค้ดบรรทัดล่าง
    print(dataset.variables.keys())

    # variables เวลา 
    times = dataset.variables['time']
    time_unit = dataset.variables["time"].getncattr('units')
    time_cal = dataset.variables["time"].getncattr('calendar')
    # variables ความกดอากาศ
    press = dataset.variables['pressure'][:]
    
    # แปลงค่า time ในอยู่ในรูปแบบ ปีเดือนวันชัวโมง
    local_time = num2date(times[:], units=time_unit, calendar=time_cal) # convert time
    datetime = ([date.strftime('%Y%m%d%H') for date in local_time])
    # print(datetime)
    
    # variable ของลม ใช้สำหรับแปลงค่าออกมาเป็น vecter 
    # ซึ่งจำเป็นจะต้องใช้ทั้ง u และ v
    u_var = FLAGS.u_data
    v_var = FLAGS.v_data
    band_counter = 0
    for i, date in enumerate(datetime):
        print(date)
        for j,pres in enumerate(press):
            print(pres)
            band_counter += 1
            print(band_counter)
            
            # query data 1 hr 1 pressure แล้วสร้างไฟล์ใหม่ 
            u_param = '''gdal_translate -of GMT NETCDF:%s:%s u_%s00.nc -b %d''' %(FLAGS.input_file, FLAGS.u_data, date, band_counter)
            v_param = '''gdal_translate -of GMT NETCDF:%s:%s v_%s00.nc -b %d''' %(FLAGS.input_file, FLAGS.v_data, date, band_counter)
            subprocess.Popen(u_param, stdout=subprocess.PIPE, shell=True).wait()
            subprocess.Popen(v_param, stdout=subprocess.PIPE, shell=True).wait()

            # อ่านไฟล์ที่สร้าง
            data_u = Dataset('u_%s00.nc'%date)
            data_v = Dataset('v_%s00.nc'%date)
            # ประกาศรูปแบบของ json
            dict=[{
                'header':{
                    "parameterNumberName": "eastward_wind",
                    "parameterUnit": "m.s-1",
                    "parameterNumber": 2,
                    "parameterCategory": 2,
                    "numberPoints": data_u.variables['z'].shape[0],
                    'nx': data_u.variables['dimension'][0].tolist(),
                    'ny': data_u.variables['dimension'][1].tolist(),
                    'lo1': data_u.variables['x_range'][0].tolist(),
                    'la1': data_u.variables['y_range'][1].tolist(),
                    'lo2': data_u.variables['x_range'][1].tolist(),
                    'la2': data_u.variables['y_range'][0].tolist(),
                    'dx': data_u.variables['spacing'][0].tolist(),
                    'dy': data_u.variables['spacing'][1].tolist(),
                },
                'data': data_u.variables['z'][:].tolist()
                },{
                'header':{
                    "parameterNumberName": "eastward_wind",
                    "parameterUnit": "m.s-1",
                    "parameterNumber": 3,
                    "parameterCategory": 2,
                    "numberPoints": data_v.variables['z'].shape[0],
                    'nx': data_v.variables['dimension'][0].tolist(),
                    'ny': data_v.variables['dimension'][1].tolist(),
                    'lo1': data_v.variables['x_range'][0].tolist(),
                    'la1': data_v.variables['y_range'][1].tolist(),
                    'lo2': data_v.variables['x_range'][1].tolist(),
                    'la2': data_v.variables['y_range'][0].tolist(),
                    'dx': data_v.variables['spacing'][0].tolist(),
                    'dy': data_v.variables['spacing'][1].tolist(),
                },
                'data': data_v.variables['z'][:].tolist()
            }]

            # เช็คว่ามี folder ลมอยู่หรือไม่ ถ้าไม่มีให้สร้าง folder ลม
            if not os.path.exists("%s/%s/wind/%s"%(FLAGS.output_dir,FLAGS.domain,pres)):
                os.makedirs("%s/%s/wind/%s"%(FLAGS.output_dir,FLAGS.domain,pres))
            # ประกาศชื่อไฟล์
            output_file_name = "%s/%s/wind/%s/wind_%s00.json"%(FLAGS.output_dir,FLAGS.domain,pres,date)
            # สร้างไฟล์ json ตามชื่อบรรทัดบน และเพิ่มข้อมูลลงไป
            with open( output_file_name, 'w') as outfile:  
                outfile.write(json.dumps(dict))

            data_u.close()
            data_v.close()

            # ลบไฟล์ netcdf ที่สร้างไว้
            rm_u_cmd = "rm -f u_%s00.nc*"%date
            rm_v_cmd = "rm -f v_%s00.nc*"%date
            subprocess.Popen(rm_u_cmd, stdout=subprocess.PIPE, shell=True).wait()
            subprocess.Popen(rm_v_cmd, stdout=subprocess.PIPE, shell=True).wait()
            rm_txt = "rm -f *.txt"
            subprocess.Popen(rm_txt, stdout=subprocess.PIPE, shell=True).wait()

            print ('[INFO] *** SUCCESS writing JSON Wind ***')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input_file',
        type=str,
        default='uv_wind_d01_current.nc',
        help='Path file input'
    )
    parser.add_argument(
        '--domain',
        type=str,
        default='domain1',
        help='Path to folders of ouput'
    )
    parser.add_argument(
        '--v_data',
        type=str,
        default='v_tr_p',
        help='Variables name of v data'
    )
    parser.add_argument(
        '--u_data',
        type=str,
        default='u_tr_p',
        help='Variables name of u data'
    )
    parser.add_argument(
        '--output_dir',
        type=str,
        default='data/geoserver/json',
        help='Path to folders of ouput'
    )
    FLAGS, unparsed = parser.parse_known_args()
    print(FLAGS)
    main()